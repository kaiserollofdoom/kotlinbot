package org.kaiserollofdoom.kotlinbot.coffee

import java.lang.IllegalArgumentException

sealed class Drinkable: Expr {
    abstract val cost: Double
    abstract val tag: String
}
data class Coffee(override val cost: Double = 1.50, override val tag: String = "org/kaiserollofdoom/kotlinbot/coffee"): Drinkable()
data class Milk(override val cost: Double = .25, override val tag: String = "addon"): Drinkable()
data class EarlGreyTea(override val cost: Double = 1.0,override val  tag: String = "tea"): Drinkable()
data class Sugar(override val cost: Double = .1,override val  tag: String = "addon"): Drinkable()
data class MatchaTea(override val cost: Double = 2.50,override val  tag: String = "tea"): Drinkable()
data class WhippedCream(override val cost: Double = .75,override val  tag: String = "addon"): Drinkable()
data class Empty(override val cost: Double = 0.0, override val tag: String = "addon"): Drinkable()

interface Expr
data class Sum(val left: Expr, val right: Expr): Expr
data class Minus(val left: Expr, val right: Expr): Expr

fun parseDrinkableHelper(name: String): Expr {
    return when(name){
        "coffee" -> Coffee()
        "earlgrey" -> EarlGreyTea()
        "earlgreytea" -> EarlGreyTea()
        "milk" -> Milk()
        "matcha" -> MatchaTea()
        "matchtea" -> MatchaTea()
        "whippedcream" -> WhippedCream()
        "whip" -> WhippedCream()
        "latte" -> Sum(Sum(Coffee(), Milk()), Sum(Sugar(), WhippedCream()))
        "sugar" -> Sugar()
        else -> throw IllegalArgumentException("Drinkable not found")
    }
}

fun calculateCost(expr: Expr): Double{
    return when(expr){
        is Drinkable -> expr.cost
        is Sum -> calculateCost(expr.left) + calculateCost(expr.right)
        is Minus -> calculateCost(expr.left) - calculateCost(expr.right)
        else -> throw IllegalArgumentException("RUHROH")
    }
}

//extensions are super useful
fun String.drinkable(): Expr = parseDrinkableHelper(this)
val <T> List<T>.tail: List<T> get() = drop(1)
val <T> List<T>.head: T get() = first()
val Expr.cost : Double get() = calculateCost(this)

fun parseDrinkOrder(drinkOrder: String): Expr {
    val orderItems = drinkOrder.split(" ")
    tailrec fun inner(itemsLeft: List<String>, expr: Expr): Expr {
        return when {
            itemsLeft.isEmpty() -> expr
            itemsLeft.head.startsWith("!") -> inner(itemsLeft.tail, Minus(expr, itemsLeft.head.drinkable()))
            else -> inner(itemsLeft.tail, Sum(expr, itemsLeft.head.drinkable()))
        }
    }
    //get the first to items
    val first = orderItems.head
    return when{
        orderItems.tail.isEmpty() -> Sum(first.drinkable(), Empty())
        orderItems.tail.head.startsWith("!") -> inner(orderItems.tail.tail, Minus(first.drinkable(), orderItems.tail.head.drop(1).drinkable()))
        else -> inner(orderItems.tail.tail, Sum(first.drinkable(), orderItems.tail.head.drinkable()))
    }
}

fun String.buildDrinkExpression(): Expr = parseDrinkOrder(this)

object DrinkableSources{
    val drinks= listOf(
            Coffee(),
            Milk(),
            EarlGreyTea(),
            Sugar(),
            MatchaTea(),
            WhippedCream()
    )
}
