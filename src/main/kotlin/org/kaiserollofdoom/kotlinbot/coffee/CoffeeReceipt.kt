package org.kaiserollofdoom.kotlinbot.coffee

import java.lang.StringBuilder

/*
Helper functions for building a markdown based receipt to push
 */

fun buildString(build: StringBuilder.() -> Unit): String {
    val stringBuilder = StringBuilder()
    stringBuilder.build()
    return stringBuilder.toString()
}

fun buildNiceReceipt(drinkOrder: String, customer: String): String {
    tailrec fun inner(itemsLeft: List<String>, builderRef: StringBuilder){
        /**
         * Helper to traverse the expression and append items to builder
         */
        when {
            itemsLeft.isEmpty() -> return
            itemsLeft.head.startsWith('!') -> {
                builderRef.append("\n")
                val head = itemsLeft.head.drop(1)
                builderRef.append("- $head")
                inner(itemsLeft.tail, builderRef)
            }
            itemsLeft.head.isNotEmpty() -> {
                builderRef.append("\n")
                val head = itemsLeft.head
                builderRef.append("+ $head")
                inner(itemsLeft.tail, builderRef)
            }
        }
    }
    return buildString{
        //Push the top
        append(customer)
        append("\n")
        append("====")
        append("\n")
        // now we have to take each item in the drink order and add them as items on the list
        val drinkSplits = drinkOrder.split(' ')
        inner(drinkSplits, this)
        val drinkCost = drinkOrder.buildDrinkExpression().cost
        append("\n")
        append("Total: $drinkCost")
    }
}
