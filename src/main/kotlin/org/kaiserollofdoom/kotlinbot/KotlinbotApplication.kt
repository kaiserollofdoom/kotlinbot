package org.kaiserollofdoom.kotlinbot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinbotApplication

fun main(args: Array<String>) {
	runApplication<KotlinbotApplication>(*args)
}

