package org.kaiserollofdoom.kotlinbot.slack

import org.kaiserollofdoom.kotlinbot.coffee.buildDrinkExpression
import org.kaiserollofdoom.kotlinbot.coffee.buildNiceReceipt
import org.kaiserollofdoom.kotlinbot.coffee.cost
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class CoffeeOrderController{

    @PostMapping("/coffeeOrder")
    fun coffeeOrder(@RequestParam("text") coffeeOrder: String, @RequestParam("user_name") customer: String) : SlackResponse {
        val coffee = coffeeOrder.buildDrinkExpression()
        val costCoffee = coffee.cost
        val markdownText = buildNiceReceipt(coffeeOrder, customer)
        return SlackResponse("in_channel", markdownText, emptyList())
    }
}