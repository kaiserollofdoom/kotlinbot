package org.kaiserollofdoom.kotlinbot.slack

data class SlackResponse(val response_type: String, val text: String, val attachments: List<SlackAttachment>)

data class SlackAttachment(val text: String, val url: String)
