package org.kaiserollofdoom.kotlinbot.coffee

import assertk.assertThat
import assertk.assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class TestCoffee {
    @Nested
    inner class TestItemParsing{
        /*
        Can we parse singular items into types
         */
        @Test
        fun `parse coffee`() {
            val coffee = "coffee"
            val expectedCoffee = coffee.drinkable()
            assertThat(expectedCoffee).isEqualTo(Coffee())

        }
        @Test
        fun `parse latte `(){
            val latte = "latte".drinkable()
            assertThat(latte).isEqualTo(Sum(Sum(Coffee(), Milk()), Sum(Sugar(), WhippedCream())))


        }
        @Test
        fun `parse tea`(){
            val earlGrey = "earlgrey".drinkable()
            val matchTea = "matcha".drinkable()
            assertThat(earlGrey).isEqualTo(EarlGreyTea())
        }
        @Test
        fun `parse optionals`(){
            val whippedCream = "whippedcream".drinkable()
            val sugar = "sugar".drinkable()
            val milk = "milk".drinkable()
            assertThat(whippedCream).isEqualTo(WhippedCream())
            assertThat(sugar).isEqualTo(Sugar())
            assertThat(milk).isEqualTo(Milk())

        }
    }
    @Nested
    inner class TestCommandParse{
        @Test
        fun `parse tea milk and sugar`(){
            val englishTea = "earlgreytea milk sugar".buildDrinkExpression()
            assertThat(englishTea).isEqualTo(Sum(Sum(EarlGreyTea(), Milk()), Sugar()))
        }
        @Test
        fun `parse latte no whip`(){
            //Weird drink, but ok
            val latteNoWhip = "latte !whippedcream".buildDrinkExpression()
            assertThat(latteNoWhip).isEqualTo(Minus("latte".drinkable(), WhippedCream()))
        }
    }

    @Nested
    inner class TestEvaluationOfDrinks{
        @Test
        fun `latte to cost`(){
            val latte = Sum(Sum(Coffee(), Milk()), Sum(Sugar(), WhippedCream()))
            val actualCost = calculateCost(latte)
            assertThat(actualCost).isEqualTo(2.6)
        }

        @Test
        fun `latte no whip`(){
            val latte = "latte !whippedcream".buildDrinkExpression()
            val actualCost = calculateCost(latte)
            assertThat(actualCost).isEqualTo(1.85)
        }

        @Test
        fun `coffee expression invoke`(){
            val latte = "latte !whippedcream".buildDrinkExpression()
            val actualCost = latte.cost
            assertThat(actualCost).isEqualTo(1.85)
        }
    }
}
