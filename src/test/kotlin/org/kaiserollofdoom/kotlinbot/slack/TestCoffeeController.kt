package org.kaiserollofdoom.kotlinbot.slack

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest
class TestCoffeeController(@Autowired val mockMvc: MockMvc){

    @Test
    fun `Order valid coffee`(){
        val mvcResult = mockMvc.perform(
                post("/api/coffeeOrder")
                        .param("token", "gIkuvaNzQIHg97ATvDxqgjtO")
                        .param("team_id", "T0001")
                        .param("team_domain", "some_team_domain")
                        .param("enterprise_id", "E0001")
                        .param("enterprise_name", "SomeTeamDomain")
                        .param("channel_id", "C2147483705")
                        .param("channel_name", "test")
                        .param("user_name", "mrkaiser")
                        .param("command", "/coffee")
                        .param("text", "latte !whippedcream sugar")
                        .param("response_url", "https://hooks.slack.com/commands/1234/5678")
                        .param("trigger_id", "13345224609.738474920.8088930838d88f008e0")
        ).andExpect(status().isOk).andReturn()

    }
}
